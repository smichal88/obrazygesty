#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Na licencji:
#	GPL
# Autorzy:
#   Tomasz Karbownicki
#   Marek Knaga
#   Jarosław Miazga

from numpy import *

class Neuron:
    wnum = 0        #weights number
    w = []          #weigths list 
    y = 0           #output value
    actFunct = 1    #activation function
    def __init__(self, wnum):
        self.wnum = wnum        
    
    def getOutput(self):
        return self.y
    
    def setOutput(self, val):
        self.y = val
        
    def setActFunct(self, fNum):
        if fNum < 1 or fNum > 3:
            self.actFunct = 1
            raise NeuronError("Activation function number should be 1-3. Setting default value: 1")
        self.actFunct = fNum
    
    def setWeights(self, liWeights):
        if len(liWeights) != self.wnum:
            raise NeuronError("Number of weights doesn't match neuron's weights number")
        self.w = liWeights
        
    def setWeight(self, wIndex, wVal):
        try:
            self.w[wIndex] = wVal
        except IndexError:
            raise NeuronError("Weight with number %d doesn't exist" % wIndex)

    def getWeight(self, wIndex):
        try:
            weight = self.w[wIndex]
        except IndexError:
            raise NeuronError("Weight with number %d doesn't exist" % wIndex)
        return weight
    
    def getWeights(self):
        return self.w
    
    def getActFunct(self):
        return self.actFunct

class Patterns:
    __vLen = 0           #vector length
    __patterns = []      #vector
    def __init__(self, vLen):
        if vLen < 1:
            self.__vLen = 1
        else:
            self.__vLen = vLen
            
    def addPattern(self, pat):
        if len(pat) == self.__vLen:
            self.__patterns.append(pat)
        else:
            raise PatternsError("Pattern is too long. Required length: %d, given pattern's length: %d" % (self.__vLen, len(pat)))
    
    def addPatternsFile(self,fileName):
        patFile = open(fileName,'r')
        ptxt = patFile.read()
        ptxt = ptxt.replace("\n"," ").replace("  "," ").strip()
        plist = ptxt.split("###")
        for p in plist:
            pattxt = p.strip()
            pat = pattxt.split(" ")
            i = 0
            for v in pat:
                pat[i] = int(v)
                i += 1
            self.addPattern(pat)
            
    def delPattern(self, patNum):
        del self.__patterns[patNum]
    
    def getPatterns(self):
        return self.__patterns
    
class HopfieldNet:
    __nOut = 0
    __b = 0.1
    __numSteps = 20
    __neurons = []
    __patterns = 0
    def __init__(self, nOut):
        if nOut <= 0:
            raise HopfieldNetError("Number of outputs must be greater then 0")
        
        i=0
        while i<nOut:
            self.__neurons.append(Neuron(nOut))
            i += 1
            
        self.__nOut = nOut
        self.setActFunct(2)
        
    def getNeurons(self):
        return self.__neurons
    
    def setActFunct(self, fNum):
        if fNum < 1 or fNum > 3:
            fNum = 1
        
        i = 0
        while i < self.__nOut:
            self.__neurons[i].setActFunct(fNum)
            i += 1

    def train(self, patterns, method="pseudoinv"):        
        if len(patterns[0]) != self.__nOut:
            raise HopfieldNetError("Length of pattern vector should be equavilent to number of outputs")
        
        self.__patterns = patterns

        if method == "hebb":
            t = array((patterns))
            W = indices((self.__nOut,self.__nOut))
            #W = (1.0/self.__nOut) * dot(transpose(t), t)        
            W = dot(transpose(t), t)
            
            i=0
            for w in W:
                self.__neurons[i].setWeights(w)
                i += 1
                
            i = 0
            try:
                while i >= 0:
                    W[i][i] = 0
                    i += 1
            except IndexError:
                i=0
                for w in W:                
                    self.__neurons[i].setWeights(w)
                    i += 1          
        
        elif method == "pseudoinv":
            ####### metoda pseudoinwersji #######
            t2 = matrix((patterns))
            W2 = indices((self.__nOut,self.__nOut))        
            invt = dot(t2, t2.T).I
            tmp = dot(t2.T, invt)
            W2 = dot(tmp, t2)
            
            W2 = array(W2)        
            i = 0
            try:
                while i >= 0:
                    W2[i][i] = 0
                    i += 1
            except IndexError:
                i=0
                for w in W2:                
                    self.__neurons[i].setWeights(w)
                    i += 1  
    
    
    def simulate(self, input):
        if len(input) != self.__nOut:
            raise HopfieldNetError("Length of input vector should be equivalent to number of net outputs")
        
        #setting up outputs
        k = 0
        while k < self.__nOut:            
            self.__neurons[k].setOutput(input[k])            
            k += 1
        
        #simulation
        oldOut = []
        steps = 0        
        while (steps <= self.__numSteps) and (not self.__cmpOutput(oldOut)): 
            oldOut = []
            for n in self.__neurons:
                oldOut.append(n.getOutput())            
            i = 0
            while i < self.__nOut:
                fi = 0
                j = 0
                while j < self.__nOut: 
                    fi += self.__neurons[i].getWeight(j) * oldOut[j]
                    j += 1
                fi += self.__b
                
                if self.__neurons[i].getActFunct() == 1:
                    if fi >= 0:
                        self.__neurons[i].setOutput(1)
                    elif fi < 0:
                        self.__neurons[i].setOutput(0)
                elif self.__neurons[i].getActFunct() == 2:
                    if fi >= 0:
                        self.__neurons[i].setOutput(1)
                    elif fi < 0:
                        self.__neurons[i].setOutput(-1)
                elif self.__neurons[i].getActFunct() == 3:
                    if fi >= 1:
                        self.__neurons[i].setOutput(1)
                    elif fi <= -1:
                        self.__neurons[i].setOutput(-1)
                    else:
                        self.__neurons[i].setOutput(fi)            
                i += 1
            steps += 1
        
        i=0
        for pat in self.__patterns:
            if self.__cmpOutput(pat) == 1:
                return i
            i += 1
            
        return -1
        
    def getOutputs(self):
        output = []
        for n in self.__neurons:
            output.append(n.getOutput())
        return output
    
    def __cmpOutput(self, oldOut):
        if len(oldOut) != self.__nOut:
            return 0
        i = 0
        for oo in oldOut:
            if oo != self.__neurons[i].getOutput():
                return 0
            i += 1
        return 1
    
########## Errors #####################
class NeuronError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
    
class PatternsError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class HopfieldNetError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
        

def printVector(v, cpl):    
    i = 0
    for val in v:
        if i < cpl:
            if val == 1 or val == 0:
                print '\033[1;31m%2d\033[1;37m' % val,
            else:
                print "%2d" % val,
            i += 1
        else:
            if val == 1 or val == 0:
                print "\n\033[1;31m%2d\033[1;37m" % val,
            else:
                print "\n%2d" % val,
            i = 1
