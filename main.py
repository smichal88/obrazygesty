#!/usr/bin/env python

import cv
from hopfieldnet import *
import alsaaudio as al
import subprocess

size=(640,480)
cam = cv.CreateCameraCapture(0)
#cam = cv.CreateCameraCapture(1)
cv.SetCaptureProperty(cam,cv.CV_CAP_PROP_FRAME_WIDTH, size[0])
cv.SetCaptureProperty(cam,cv.CV_CAP_PROP_FRAME_HEIGHT, size[1])


cascade = cv.Load("haarcascade_hand.xml")

cv.NamedWindow("Test", cv.CV_WINDOW_AUTOSIZE)

gestp = []
for i in range(192):
                gestp.append(-1)

gest = gestp[:]
p = Patterns(192)
hn = HopfieldNet(192) 

p.addPatternsFile("innepat.txt")
hn.train(p.getPatterns())

tracklist = [(10,10)]

process = None #dla subprocess
motionVector = []
while True:
        image = cv.QueryFrame(cam)
        cv.Smooth(image,image,cv.CV_GAUSSIAN,3,3)
        detectedObject = cv.HaarDetectObjects(image, cascade, cv.CreateMemStorage(0), 1.2,2,0,(40,40))
        for i in detectedObject:
                req = i[0]
                x = req[0] + req[2]/2
                y = req[1] + req[3]/2
                motionVector.append((x,y))
#                cv.Rectangle(image, (req[0], req[1]), (req[0]+req[2], req[1]+req[3]), cv.Scalar(255,0,0))
#        print motionVector
        if motionVector:
                last = motionVector[0]
                for j in motionVector:
                        cv.Circle(image,j,5, cv.Scalar(0,0,255),-1)
                        last = j
        if not detectedObject:
                for j in motionVector:
                        x = ceil(j[1]/40)
                        y = ceil(j[0]/40)
                        pos = int((x-1)*16+(y-1))
                        gest[pos] = 1
                if motionVector:
                        print
                        #print "Wektor ruchu"
                        #printVector(gest,16)
                        #print
                        #print "\nRozpoznano:\n"
                        lol = hn.simulate(gest)

                        #print 
                        #print "Siec odpowiedziala:"
                        #printVector(hn.getOutputs(),16)
                        #print

                        if lol != -1:
                                print "Rozpoznano gest"
                                print lol
                                if lol == 0:
                                        print "podglosnie dzwiek"
                                        al.Mixer().setvolume(100)
                                elif lol == 1:
                                        print "wycisze dzwiek"
                                        al.Mixer().setvolume(0)
                                elif lol == 2:
                                        print "wlaczam mplayer"
                                        if process == None:
                                                process =  subprocess.Popen(['mplayer','Kabaret.mp4'])
                                        else:
                                                process.kill()
                                                process = None

                                elif lol == 3:
                                        print "wylaczam  mplayera"
                                        if process != None:
                                                process.kill()
                                                process = None

                motionVector = []
                gest = gestp[:]



        cv.Flip(image,image,1)
        cv.ShowImage("Test",image)
        if cv.WaitKey(5) != -1:
                break;
